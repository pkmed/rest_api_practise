<?php

namespace App\Controller\API;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api", name="api_")
 */
class UsersController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/users")
     */
    public function getUserAction()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        if (empty($users)) {
            return $this->view('There are no movies', Response::HTTP_NOT_FOUND);
        }

        return [$users, Response::HTTP_OK];
    }
}
