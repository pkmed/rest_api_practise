<?php

namespace App\Controller\API;

use App\Entity\Movie;
use App\Form\MovieType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class MovieController extends AbstractFOSRestController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Movies.
     *
     * @Rest\Get("/movies")
     */
    public function getMovieAction()
    {
        $movies = $this->getDoctrine()->getRepository(Movie::class)->findAll();
        if (empty($movies)) {
            return $this->view('There are no movies', Response::HTTP_NOT_FOUND);
        }

        return $this->view($movies, Response::HTTP_OK);
    }

    /**
     * Create Movie.
     *
     * @Rest\Post("/movie")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postMovieAction(Request $request)
    {
        $movie = new Movie();
        $form = $this->createForm(MovieType::class, $movie);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($movie);
            $this->entityManager->flush();

            return $this->view(['status' => 'ok'], Response::HTTP_CREATED);
        }

        return $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Update movie info.
     *
     * @Rest\Put("/movie/{id}")
     *
     * @param Movie   $movie
     * @param Request $request
     *
     * @return View
     */
    public function putMovieFunction(Movie $movie, Request $request)
    {
        $form = $this->createForm(MovieType::class, $movie);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($movie);
            $this->entityManager->flush();

            return $this->view('info updated', Response::HTTP_ACCEPTED);
        }

        return $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete movie.
     *
     * @Rest\Delete("/movie/{id}")
     *
     * @param Movie $movie
     *
     * @return View
     */
    public function deleteMovieFunction(Movie $movie)
    {
        if ($movie) {
            $this->entityManager->remove($movie);
            $this->entityManager->flush();

            return $this->view('movie deleted', Response::HTTP_ACCEPTED);
        }

        return $this->view('movie not found', Response::HTTP_NOT_FOUND);
    }
}
