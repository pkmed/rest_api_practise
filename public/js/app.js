let moviesListElem;
let idField;
let titleField;
let descriptionField;
let refreshMoviesBtn;

const moviesList = new Map();


$(document).ready(async () => {
    moviesListElem = $('#movies');
    idField = $("#movieId");
    titleField = $('#movieTitle');
    descriptionField = $('#movieDescription');
    refreshMoviesBtn = $('#refreshMoviesBtn');

    refreshMoviesBtn.on({
        mouseenter: () => {
            refreshMoviesBtn.css('background-color', '#ABC');
        },
        mouseleave: () => {
            refreshMoviesBtn.css('background-color', '');
        },
        click: () => {
            refreshMoviesList();
        }
    });

    refreshMoviesList();

});

function formBtnHandler(btn) {
    let movieData;
    switch (btn.innerText) {
        case 'create':
            movieData = {name: titleField.val(), description: descriptionField.val()};
            createMovie(JSON.stringify(movieData));
            break;
        case 'Update info':
            movieData = moviesList.get(parseInt(idField.val()));
            movieData.name = titleField.val();
            movieData.description = descriptionField.val();
            updateMovie(movieData).always((data, textStatus, xhr) => {
                alert(xhr.responseText);
            });
            break;
        case 'Delete':
            deleteMovie(idField.val()).always((data, textStatus, xhr) => {
                alert(xhr.responseText);
            });
            break;
    }

    btn.innerText = 'create';
    titleField.val('');
    descriptionField.val('');
    refreshMoviesList();
    $('#movieFormDelBtn').remove();
}

async function refreshMoviesList() {
    let movies = await getAllMovies();
    moviesListElem.empty();
    for (const movie of movies) {
        moviesList.set(movie.id, Object.assign(new MovieRow(), movie));
        moviesListElem.append(Object.assign(new MovieRow(), movie).view);
    }
}

/***************************/
/*      api requests       */
/***************************/
function getAllMovies() {
    return $.get("/api/movies");
}

function createMovie(movieData) {
    return $.post("/api/movie", movieData);
}

function updateMovie(movieData) {
    return $.ajax({
        url: `/api/movie/${movieData.id}`,
        type: 'PUT',
        data: JSON.stringify({name: movieData.name, description: movieData.description}),
        dataType: 'json'
    });
}

function deleteMovie(id) {
    return $.ajax({
        url: `/api/movie/${id}`,
        type: 'DELETE'
    });
}