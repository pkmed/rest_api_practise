class MovieRow {
    constructor(id, name, description) {
        this._id = id;
        this._name = name;
        this._description = description;
        this._view = $("<li></li>").addClass('movieRow').text(name + ' - ' + description).click(() => {
            this._view.toggleClass('movieRowClicked');
            $('li.movieRowClicked').not(this._view).toggleClass('movieRowClicked');
            idField.val(this._id);
            titleField.val(this._name);
            descriptionField.val(this._description);
            $('#movieFormBtn').text('Update info');
            if ($("#movieFormDelBtn").val() !== '') {
                $("#movieForm").append($("<button id='movieFormDelBtn'>Delete</button>").addClass("btn btn-danger mt-2").click(() => {
                    formBtnHandler($("#movieFormDelBtn")[0])
                }));
            }
        });
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
        this.updateInstanceView();
    }

    get description() {
        return this._description;
    }

    set description(description) {
        this._description = description;
        this.updateInstanceView();
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get view() {
        return this._view;
    }

    updateInstanceView() {
        this.view.text(this._name + ' - ' + this._description);
    }
}